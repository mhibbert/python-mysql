from database.mysql import MySQLDatabase
from settings import database

if __name__ == "__main__":
    db = MySQLDatabase(database.get('name'), 
                       database.get('username'), 
                       database.get('password'),
                       database.get('host'))
    
    #db.get_available_tables()
    #print db.tables
    
    #for row in db.tables:
        #print "\n\n====== %s ======" % row[0]  
        #print db.get_columns_for_table(row[0])
        #for column in db.get_columns_for_table(row[0]):
            #print column[0]
        #print "--------------------------------------"     

    results = db.select('people')
    print results
    
    for row in results:
        print row
        
    results = db.select('people', columns=['id', 'first_name'], named_tuples=True)
        
    for row in results:
        print row.id, row.first_name    
        
    results = db.select('orders', 
                        named_tuples=True)
        
    print results
    
    for row in results:
        print row.id, row.amount        
        
    people = db.select('people', 
                        columns=["concat(first_name, ' ', second_name) as full_name",
                                 "SUM(amount) as total_spend"],
                        named_tuples=True,
                        where="people.id=2",
                        join="orders ON people.id=orders.person_id")
                
    for person in people:
        print person.full_name, " spent ", person.total_spend  
        
    db.insert('orders', 
              person_id=2, 
              amount=120.00)
    
    person = db.select('people', named_tuples=True)[0]
    
    db.update('profile', 
              where="person_id=%s" % person.id,
              address="1a another street")
    
    person = db.select('people', named_tuples=True)[0]
        
    db.delete('orders', 
              person_id="=%s" % person.id,
              id="=1")   
       
        
    
    
    